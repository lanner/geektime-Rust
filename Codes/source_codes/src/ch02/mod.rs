//! 第二章：Rust核心概念
//!
//! 本章包括：
//! 1. 安全管理
//! 2. 工程能力
//! 3. 元编程
//! 4. 安全边界

// section 01
pub mod s1_ownership;
// section 02
pub mod s2_lifetime;
// section 03
pub mod s3_trait_and_generic;
// section 04
pub mod s4_paradigms;
// section 05
pub mod s5_error_handle;
// section 06
pub mod s6_metaprogramming;
// section 07
pub mod s7_unsafe_rust;